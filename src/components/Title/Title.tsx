import React from "react";

interface IProps {
  as: React.ElementType;
}
export const Title = (props: IProps) => {
  const { as: Cmp = "h1", ...rest } = props;

  return <Cmp {...rest} />;
};

export default Title;
