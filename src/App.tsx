import CardBig from "./components/Card/CardBig/CardBig.tsx";
import "./main.css"
import Layout from "./components/Layout/Layout.tsx";

function App() {
  return (
    <>
      <Layout>
        <Layout>
          <CardBig src={"/programmer-kirill.jpg"} fullname={"Богомолов Кирилл Алексеевич"}/>
        </Layout>
      </Layout>
    </>
  )
}

export default App
