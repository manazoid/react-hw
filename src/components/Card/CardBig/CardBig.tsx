import classes from './CardBig.module.css'

const CardBig = ({src, fullname}) => {
  return (
    <div className={classes.card_wrapper}>
      <img src={src} alt={fullname}/>
      <p>{fullname}</p>
    </div>
  );
};

export default CardBig;
